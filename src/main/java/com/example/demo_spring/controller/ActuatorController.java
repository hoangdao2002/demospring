package com.example.demo_spring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class ActuatorController {

  @GetMapping(value = "/ping")
  public ResponseEntity<String> ping() {
    return new ResponseEntity<>("pong", HttpStatus.OK);
  }

  @GetMapping(value = "/sum")
  public ResponseEntity<Integer> sum(@RequestParam int a, @RequestParam int b) {
    return new ResponseEntity<>(a + b, HttpStatus.OK);
  }


}
